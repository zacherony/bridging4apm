const { resolve } = require("path");

// ambil data kredential dari .env di folder conf
require("dotenv").config({ path: resolve(__dirname, "./config/.env") });

const express = require("express");

// butuh validasi data, kita pakai lib express validator
// const {body, validationResult } = require('express-validator');

// panggil express js yg di alias kan sebagai app
const app = express();
app.use(express.urlencoded({ extended : true }));
app.use(express.json());

// const bodyParser = require("body-parser");

// panggil lib cors supaya app lain di server luar, bisa memanggil service dari app ini
const cors = require("cors");

// ambil angka port  dari file .env
const port = process.env.port || 8881;

// check DB
// const { promisePool } = require('./config/db')


process.env.TZ = "Asia/Jakarta";

// integrasikan cors yg sudah di panggil di atas tadi ke dalam express
app.use(cors());

// aktifkan middleware express json dan urlencoded untuk menghandle pertukaran data
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get('/', (req, res) => {
  res.send('Hello RSB1')
})

require("./routes/rVclaim")(app);



app.listen(port, () => {
  console.log(`APM-service listening at http://localhost:${port}`);
});
