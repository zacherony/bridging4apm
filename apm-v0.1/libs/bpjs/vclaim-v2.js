const axios = require("axios");
const cryptojs = require("crypto-js");
const crypto = require("crypto");
// const sec = require("../../helpers/kerip");

const usr = process.env.VCLAIM_PROD_USER_KEY;
// const usr = process.env.VCLAIM_DEV_USER_KEY;

const url = process.env.VCLAIM_PROD_URL;
// const url = process.env.VCLAIM_DEV_URL;

const url_antrol = process.env.AP_URL
const usr_antrol = process.env.AP_USER_KEY

//PRODUCTION
const cons = process.env.CONSID
const sign = process.env.SECRET

// DVLP
// const cons = sec.decryptWithAES(process.env.CONSDVLP);
// const sign = sec.decryptWithAES(process.env.SECRETDVLP);

const buatHeader = (contentType, timeStamp) => {
  // const timeInSeconds = parseInt(new Date() / 1000);
  const sigString = `${cons}&${timeStamp}`;
  const token = cryptojs.HmacSHA256(sigString, sign);
  const hash = cryptojs.enc.Base64.stringify(token);

  let config = {
    headers: {
      "Content-Type": contentType,
      "X-cons-id": cons,
      "X-timestamp": timeStamp,
      "X-signature": hash,
      user_key: usr,
    },
  };
  // console.log(config)
  return config;
};

const buatHeader_antrol = (contentType, timeStamp) => {
  // const timeInSeconds = parseInt(new Date() / 1000);
  const sigString = `${cons}&${timeStamp}`;
  const token = cryptojs.HmacSHA256(sigString, sign);
  const hash = cryptojs.enc.Base64.stringify(token);

  let config = {
    headers: {
      "Content-Type": contentType,
      "X-cons-id": cons,
      "X-timestamp": timeStamp,
      "X-signature": hash,
      user_key: usr_antrol,
    },
  };
  // console.log(config)
  return config;
};

/* credit to @setsuga */
exports.stringDecryptV2 = ( string, ts ) => {
  // const stringex = string.replace(/\0/g, '')
  const KEY = cons.concat( sign, ts )
  const key_hash = crypto.createHash("sha256").update( KEY, "utf8").digest();
  const iv = key_hash.slice(0, 16, "blob");
  try {
    const decipher = crypto.createDecipheriv("aes-256-cbc", key_hash, iv);
    const decrypted = decipher.update(string, "base64", "utf8");
    return decrypted + decipher.final("utf8");
  } catch (e) {
    return e.message;
  }
}

/* ###################################################################
   ###################################################################
   ##                       VCLAIM BPJS V.2                         ##
  ####################################################################
  #################################################################### */


/* -------------------------------------------------------------------
     PESERTA (2) endpoint
  ------------------------------------------------------------------- */

// get peserta bpjs by Noka

exports.mintaDataNoka = async (noka, tgl, ts, contentType = "application/json") => {
    const resp = await axios.get(url + `/Peserta/nokartu/${noka}/tglSEP/${tgl}`, buatHeader(contentType, ts));
    console.log(`/Peserta/nokartu/${noka}/tglSEP/${tgl}`);
    return resp.data;
};

// get peserta bpjs by NIK 

exports.mintaDataNik = async (nik, tgl, ts, contentType = "application/json") => {
    const resp = await axios.get(url + `/Peserta/nik/${nik}/tglSEP/${tgl}`, buatHeader(contentType, ts));
    console.log(`/Peserta/nik/${nik}/tglSEP/${tgl}`);
    return resp.data;
};

/* -------------------------------------------------------------------
     REFERENSI (16) endpoint, (4) remaining
  ------------------------------------------------------------------- */

// get referensi diagnosa

exports.mintaRefDiagnosa = async (kodedx, ts, contentType = "application/json") => {
   const resp = await axios.get(url + `/referensi/diagnosa/${kodedx}`, buatHeader(contentType, ts));
   // console.log(resp);
   return resp.data;
};

// get referensi poliklinik

exports.mintaRefPoli = async (poli, ts, contentType = "application/json") => {
    const resp = await axios.get(url + `/referensi/poli/${poli}`, buatHeader(contentType, ts));
    // console.log(resp);
    return resp.data;
};

// get referensi Faskes

exports.mintaRefFaskes = async (faskes, jenis, ts, contentType = "application/json") => {
    const resp = await axios.get(url + `/referensi/faskes/${faskes}/${jenis}`, buatHeader(contentType, ts));
    // console.log(resp);
    return resp.data;
};

// get referensi Dokter DPJP

exports.mintaRefDpjp = async (jenpel, tglayan, kdspesialis, ts, contentType = "application/json") => {
    const resp = await axios.get(url + `/referensi/dokter/pelayanan/${jenpel}/tglPelayanan/${tglayan}/Spesialis/${kdspesialis}`, buatHeader(contentType, ts));
    // console.log(resp);
    return resp.data;
};

/* -------------------------------------------------------------------
     RUJUKAN
  ------------------------------------------------------------------- */

  exports.mintaListRujByTgl = async (tgl, ts, contentType = "application/json") => {
    const resp = await axios.get(url + `Rujukan/List/TglRujukan/${tgl}`, buatHeader(contentType, ts));
    console.log(`/Rujukan/List/TglRujukan/${tgl}`);
    return resp.data;
};

  exports.getRujukanByNoRujukan = async (param1, ts, contentType = "application/json; charset=utf-8") => {
    const resp = await axios.get(url + `/Rujukan/${param1}`, buatHeader(contentType, ts));
    console.log(`/Rujukan/${param1}`);
    return resp.data;
  };

  exports.getRujukanByNoKaSingle = async (param1, ts, contentType = "application/json; charset=utf-8") => {
    const resp = await axios.get(url + `/Rujukan/Peserta/${param1}`, buatHeader(contentType, ts));
    console.log(`/Rujukan/Peserta/${param1}`);
    return resp.data;
  };
  
  exports.getRujukanByNoKaMulti = async (param1, ts, contentType = "application/json; charset=utf-8") => {
    const resp = await axios.get(url + `/Rujukan/List/Peserta/${param1}`, buatHeader(contentType, ts));
    console.log(`/Rujukan/List/Peserta/${param1}`);
    return resp.data;
  };

/* -------------------------------------------------------------------
     SEP
  ------------------------------------------------------------------- */

  exports.buatSEPv1 = async (data, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.post(url + `/SEP/1.1/insert`, data, buatHeader(contentType, ts));
    console.log(`/SEP/2.0/insert`);
    return resp.data;
  };

  exports.buatSEPv2 = async (data, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.post(url + `/SEP/2.0/insert`, data, buatHeader(contentType, ts));
    console.log(`/SEP/2.0/insert`);
    return resp.data;
  };

  exports.updateSEPv2 = async (data, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.put(url + `/SEP/2.0/update`, data, buatHeader(contentType, ts));
    console.log(`/SEP/2.0/update`);
    return resp.data;
  };

  exports.deleteSEPv2 = async (data, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.delete(url + `/SEP/2.0/delete`, data, buatHeader(contentType, ts));
    console.log(`/SEP/2.0/delete`);
    return resp.data;
  };

  exports.getSEP = async (param1, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.get(url + `/SEP/${param1}`, buatHeader(contentType, ts));
    console.log(`/SEP/${param1}`);
    return resp.data;
  };

  exports.getSingleFp = async (param1, param2, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.get(url + `/SEP/FingerPrint/Peserta/${param1}/TglPelayanan/${param2}`, buatHeader(contentType, ts));
    console.log(`/SEP/FingerPrint/Peserta/${param1}/TglPelayanan/${param2}`);
    return resp.data;
  };  

  exports.getListFp = async (param1, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.get(url + `/SEP/FingerPrint/List/Peserta/TglPelayanan/${param1}`, buatHeader(contentType, ts));
    console.log(`/SEP/FingerPrint/List/Peserta/TglPelayanan/${param1}`);
    return resp.data;
  };  

  exports.updateTglPulangv2 = async (data, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.put(url + `/SEP/2.0/updtglplg`, data, buatHeader(contentType, ts));
    console.log(`/SEP/2.0/updtglplg`);
    return resp.data;
  };

  exports.getlistTglPulangv2 = async (param1, param2, param3, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.get(url + `/Sep/updtglplg/list/bulan/${Param1}/tahun/${Param2}/${Param3}`, buatHeader(contentType, ts));
    console.log(`/Sep/updtglplg/list/bulan/${Param1}/tahun/${Param2}/${Param3}`);
    return resp.data;
  };

  /* -------------------------------------------------------------------
     MONITORING (4) endpoint, (3) remaining
  ------------------------------------------------------------------- */

  exports.getHistoryPelayanan = async (param1, param2, param3, ts, contentType = "application/json; charset=utf-8") => {
    const resp = await axios.get(url + `/monitoring/HistoriPelayanan/NoKartu/${param1}/tglMulai/${param2}/tglAkhir/${param3}`, buatHeader(contentType, ts));
    console.log(`/monitoring/HistoriPelayanan/NoKartu/${param1}/tglMulai/${param2}/tglAkhir/${param3}`);
    return resp.data;
  };
  
  exports.getKunjungan = async (param1, param2, ts, contentType = "application/json; charset=utf-8") => {
    const resp = await axios.get(url + `/Monitoring/Kunjungan/Tanggal/${param1}/JnsPelayanan/${param2}`, buatHeader(contentType, ts));
    console.log(`/Monitoring/Kunjungan/Tanggal/${param1}/JnsPelayanan/${param2}`);
    return resp.data;
  };

  /* -------------------------------------------------------------------
     Rencana Kontrol (11) endpoint, (6) remaining
  ------------------------------------------------------------------- */


  exports.postInsRencKontrol = async (data, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.post(url + `/RencanaKontrol/insert`, data, buatHeader(contentType, ts));
    console.log(`/RencanaKontrol/insert`);
    return resp.data;
  };

  exports.postUpdateRencKontrol = async (data, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.put(url + `/RencanaKontrol/Update`, data, buatHeader(contentType, ts));
    console.log(`/RencanaKontrol/Update`);
    return resp.data;
  };

  exports.postDeleteRencKontrol = async (data, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.delete(url + `/RencanaKontrol/Delete`, data, buatHeader(contentType, ts));
    console.log(`/RencanaKontrol/Delete`);
    return resp.data;
  };

  exports.getNoSuratKontrol = async (param1, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.get(url + `/RencanaKontrol/noSuratKontrol/${param1}`, buatHeader(contentType, ts));
    console.log(`/RencanaKontrol/noSuratKontrol/${param1}`);
    return resp.data;
  };

  exports.getListRencanaKontrol = async (param1, param2, param3, param4, ts, contentType = "application/json; charset=utf-8") => {
    const resp = await axios.get(url + `/RencanaKontrol/ListRencanaKontrol/Bulan/${param1}/Tahun/${param2}/Nokartu/${param3}/filter/${param4}`, buatHeader(contentType, ts));
    console.log(`/RencanaKontrol/ListRencanaKontrol/Bulan/${param1}/Tahun/${param2}/Nokartu/${param3}/filter/${param4}`);
    return resp.data;
  };

  exports.getListSpesialistik = async (param1, param2, param3, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.get(url + `/RencanaKontrol/ListSpesialistik/JnsKontrol/${param1}/nomor/${param2}/TglRencanaKontrol/${param3}`, buatHeader(contentType, ts));
    console.log(`/RencanaKontrol/ListSpesialistik/JnsKontrol/${param1}/nomor/${param2}/TglRencanaKontrol/${param3}`);
    return resp.data;
  };

  exports.getJadwalPraktekDokter = async (param1, param2, param3, ts, contentType = "Application/x-www-form-urlencoded") => {
    const resp = await axios.get(url + `/RencanaKontrol/JadwalPraktekDokter/JnsKontrol/${param1}/KdPoli/${param2}/TglRencanaKontrol/${param3}`, buatHeader(contentType, ts));
    console.log(`/RencanaKontrol/JadwalPraktekDokter/JnsKontrol/${param1}/KdPoli/${param2}/TglRencanaKontrol/${param3}`);
    return resp.data;
  };



  
  /* -------------------------------------------------------------------
     Antrol
  ------------------------------------------------------------------- */
  // get jadwal dokter

exports.jadwalDokter = async (kodepoli, tanggal, ts, contentType = "application/json") => {
  const resp = await axios.get(url_antrol + `/jadwaldokter/kodepoli/${kodepoli}/tanggal/${tanggal}`, buatHeader_antrol(contentType, ts));
  // console.log(resp);
  return resp.data;
};