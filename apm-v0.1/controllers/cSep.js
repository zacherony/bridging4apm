
// untuk men deccompress data rest api dari bpjs
const { lzDecompress } = require('../helpers/helper') 

// untuk men decrypt data rest api dari bpjs
const { stringDecryptV2 } = require('../libs/bpjs/vclaim-v2')

// endpoint sep
const  { 
    mintaDataNoka,
    mintaDataNik,
    getRujukanByNoKaSingle,
    getRujukanByNoRujukan,
    mintaRefPoli,
    mintaRefDiagnosa,
    mintaRefDpjp,
    jadwalDokter,
    buatSEPv2,
    updateSEPv2,
    deleteSEPv2
} = require('../libs/bpjs/vclaim-v2')

const response = require('../res')

//--------------------------------------------------------------------------------------
exports.getPesertaByNoka = async(req,res) => {
    const noka = req.params['noka']
    const tgl = req.params['tgl']
    //siapkan variabel 'response' untuk menampung hasil kembalian dari api
    let hasil = null
    const timeInSeconds = parseInt(new Date() / 1000);

    try {
        hasil = await mintaDataNoka( noka, tgl, timeInSeconds )        
        // console.log(hasil)
        if ( hasil.metaData.code == "200" ) {
            const decrypted = await stringDecryptV2( hasil.response, timeInSeconds )
            const jsonData = await JSON.parse( lzDecompress( decrypted ) )
            console.log("hasil: ", hasil )
            console.log( "----------------------------" )
            return res.status(200).json( jsonData )
        } else {
            console.log( hasil.metaData.code, hasil.metaData.message )
            console.log( "----------------------------" )
            return res.status( parseInt(hasil.metaData.code) ).json( hasil.metaData );
        }

    } catch (error) {
        console.log("error", error)
        return res.status(201).json({ message: error.message });
    }    
}

//--------------------------------------------------------------------------------------
exports.getPesertaByNik = async(req, res) => {
    const nik = req.params['nik']
    const tgl = req.params['tgl']

    //siapkan variabel 'response' untuk menampung hasil kembalian dari api
    let hasil = null
    const timeInSeconds = parseInt(new Date() / 1000);

    try {
        hasil = await mintaDataNik( nik, tgl, timeInSeconds ) 
        // console.log(hasil)
        if ( hasil.metaData.code == "200" ) {
            const decrypted = await stringDecryptV2( hasil.response, timeInSeconds )
            const jsonData = await JSON.parse( lzDecompress( decrypted ) )
            console.log("hasil: ", hasil )
            console.log( "----------------------------" )
            return res.status(200).json( jsonData )
        } else {
            console.log( hasil.metaData.code, hasil.metaData.message )
            console.log( "----------------------------" )
            return res.status( parseInt(hasil.metaData.code) ).json( hasil.metaData );
        }

    } catch (error) {
        console.log("error", error)
        return res.status(201).json({ message: error.message });
    }    
}

//--------------------------------------------------------------------------------------
exports.getRefPoli = async(req, res) => {
    const poli = req.params['poli']

    //siapkan variabel 'response' untuk menampung hasil kembalian dari api
    let hasil = null
    const timeInSeconds = parseInt(new Date() / 1000);

    try {
        hasil = await mintaRefPoli( poli, timeInSeconds ) 
        // console.log(hasil)
        if ( hasil.metaData.code == "200" ) {
            const decrypted = await stringDecryptV2( hasil.response, timeInSeconds )
            const jsonData = await JSON.parse( lzDecompress( decrypted ) )
            console.log("hasil: ", hasil )
            console.log( "----------------------------" )
            return res.status(200).json( jsonData )
        } else {
            console.log( hasil.metaData.code, hasil.metaData.message )
            console.log( "----------------------------" )
            return res.status( parseInt(hasil.metaData.code) ).json( hasil.metaData );
        }

    } catch (error) {
        console.log("error", error)
        return res.status(201).json({ message: error.message });
    }    
}


//--------------------------------------------------------------------------------------
exports.getRefdpjp = async(req, res) => {
    const dpjp = req.params['kddpjp']
    const jenpel = req.params['jenpel']
    const tgl = req.params['tgl']

    //siapkan variabel 'response' untuk menampung hasil kembalian dari api
    let hasil = null
    const timeInSeconds = parseInt(new Date() / 1000);

    try {
        hasil = await mintaRefDpjp( jenpel, tgl, dpjp, timeInSeconds ) 
        // console.log(hasil)
        if ( hasil.metaData.code == "200" ) {
            const decrypted = await stringDecryptV2( hasil.response, timeInSeconds )
            const jsonData = await JSON.parse( lzDecompress( decrypted ) )
            console.log("hasil: ", hasil )
            console.log( "----------------------------" )
            return res.status(200).json( jsonData )
        } else {
            console.log( hasil.metaData.code, hasil.metaData.message )
            console.log( "----------------------------" )
            return res.status( parseInt(hasil.metaData.code) ).json( hasil.metaData );
        }

    } catch (error) {
        console.log("error", error)
        return res.status(201).json({ message: error.message });
    }    
}

//--------------------------------------------------------------------------------------
exports.getRefdx = async(req, res) => {
    const poli = req.params['dx']

    //siapkan variabel 'response' untuk menampung hasil kembalian dari api
    let hasil = null
    const timeInSeconds = parseInt(new Date() / 1000);

    try {
        hasil = await mintaRefDiagnosa( dx, timeInSeconds ) 
        // console.log(hasil)
        if ( hasil.metaData.code == "200" ) {
            const decrypted = await stringDecryptV2( hasil.response, timeInSeconds )
            const jsonData = await JSON.parse( lzDecompress( decrypted ) )
            console.log("hasil: ", hasil )
            console.log( "----------------------------" )
            return res.status(200).json( jsonData )
        } else {
            console.log( hasil.metaData.code, hasil.metaData.message )
            console.log( "----------------------------" )
            return res.status( parseInt(hasil.metaData.code) ).json( hasil.metaData );
        }

    } catch (error) {
        console.log("error", error)
        return res.status(201).json({ message: error.message });
    }    
}


//--------------------------------------------------------------------------------------
exports.getJadwalDokter = async(req, res) => {
    const kdpoli = req.params['kdpoli']
    const tgl = req.params['tgl']

    //siapkan variabel 'response' untuk menampung hasil kembalian dari api
    let hasil = null
    const timeInSeconds = parseInt(new Date() / 1000);

    try {
        hasil = await jadwalDokter( kdpoli, tgl, timeInSeconds ) 
        console.log(hasil)
        if ( hasil.metadata.code == "200" || hasil.metadata.code == 200 ) {
            const decrypted = await stringDecryptV2( hasil.response, timeInSeconds )
            const jsonData = await JSON.parse( lzDecompress( decrypted ) )
            console.log("hasil: ", hasil )
            console.log( "----------------------------" )
            return res.status(200).json( jsonData )
        } else {
            console.log( hasil.metadata.code, hasil.metadata.message )
            console.log( "----------------------------" )
            return res.status( parseInt(hasil.metadata.code) ).json( hasil.metadata );
        }

    } catch (error) {
        console.log("error", error)
        return res.status(201).json({ message: error.message });
    }    
}

//--------------------------------------------------------------------------------------
exports.getRujukanByNokaSingle = async(req, res) => {
    const noka = req.params['noka']

    //siapkan variabel 'response' untuk menampung hasil kembalian dari api
    let hasil = null
    const timeInSeconds = parseInt(new Date() / 1000);

    try {
        hasil = await getRujukanByNoKaSingle( noka, timeInSeconds ) 
        console.log(hasil)
        if ( hasil.metaData.code == "200" || hasil.metaData.code == 200 ) {
            const decrypted = await stringDecryptV2( hasil.response, timeInSeconds )
            const jsonData = await JSON.parse( lzDecompress( decrypted ) )
            console.log("hasil: ", hasil )
            console.log( "----------------------------" )
            return res.status(200).json( jsonData )
        } else {
            console.log( hasil.metaData.code, hasil.metaData.message )
            console.log( "----------------------------" )
            return res.status( parseInt(hasil.metaData.code) ).json( hasil.metaData );
        }

    } catch (error) {
        console.log("error", error)
        return res.status(201).json({ message: error.message });
    }    
}

//--------------------------------------------------------------------------------------
exports.getRujukanByNoruj = async(req, res) => {
    const noruj = req.params['noruj']

    //siapkan variabel 'response' untuk menampung hasil kembalian dari api
    let hasil = null
    const timeInSeconds = parseInt(new Date() / 1000);

    try {
        hasil = await getRujukanByNoRujukan( noruj, timeInSeconds ) 
        console.log(hasil)
        if ( hasil.metaData.code == "200" || hasil.metaData.code == 200 ) {
            const decrypted = await stringDecryptV2( hasil.response, timeInSeconds )
            const jsonData = await JSON.parse( lzDecompress( decrypted ) )
            console.log("hasil: ", hasil )
            console.log( "----------------------------" )
            return res.status(200).json( jsonData )
        } else {
            console.log( hasil.metaData.code, hasil.metaData.message )
            console.log( "----------------------------" )
            return res.status( parseInt(hasil.metaData.code) ).json( hasil.metaData );
        }

    } catch (error) {
        console.log("error", error)
        return res.status(201).json({ message: error.message });
    }    
}

//--------------------------------------------------------------------------------------
exports.createSEPv2 = async (req, res) => {
    const postdata = req.body ? req.body : null
    // console.log(postdata)
    
    //siapkan variabel 'response' untuk menampung hasil kembalian dari api
    let hasil = null
    const timeInSeconds = parseInt(new Date() / 1000);

    try {
        hasil = await buatSEPv2( postdata, timeInSeconds )        
        // console.log(hasil)
        if ( hasil.metaData.code == "200" ) {
            const decrypted = await stringDecryptV2( hasil.response, timeInSeconds )
            const jsonData = await JSON.parse( lzDecompress( decrypted ) )
            console.log("hasil: ", hasil )
            console.log( "----------------------------" )
            return res.status(200).json( jsonData )
        } else {
            console.log( hasil.metaData.code, hasil.metaData.message )
            console.log( "----------------------------" )
            return res.status( parseInt(hasil.metaData.code) ).json( hasil.metaData );
        }

    } catch (error) {
        console.log("error", error)
        return res.status(201).json({ message: error.message });
    }
}