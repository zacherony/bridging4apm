'use strict';

  // -----------------------------------------------------------------
  // sukses responses
  // -----------------------------------------------------------------

exports.createTokenSukses = (values, res) => {
  var data =    {
                    "response" : { "token" : values },
                    "metadata" : { "message" : "Ok", "code" : 200 }
                } 

  res.json(data);
  res.end();
};

exports.ok = function(values, res) {
    var data = {
        "response" : values,
        "metadata" : { "message" : "Ok", "code" : 200 }
    }

    res.status(200).json(data);    
    res.end();
  };

  exports.okWithNoRespon = function(res) {
    var data = {
        "metadata" : { "message" : "Ok", "code" : 200 }
    }

    res.status(200).json(data);    
    res.end();
  };
  
  exports.entity = function(values, res) {
    var data = {
        "response" : null,
        "metadata" : { "message" : values, "code" : 201 }
    }

    res.status(201).json(data);    
    res.end();
  };

  // -----------------------------------------------------------------
  // error responses
  // -----------------------------------------------------------------

exports.noAuth = function(values, res) {
    var data = {
        "response" : { "data" : null },
        "metadata" : { "message" : values, "code" : 401 }
    }

    res.status(401).json(data);
    res.end();
}

exports.notFound = function(values, res) {
  var data = {
      "response" : { "data" : null },
      "metadata" : { "message" : values, "code" : 404 }
  }

  res.status(404).json(data);
  res.end();
}

exports.badRequest = function(values, res) {
  var data = {
      "response" : { "data" : null },
      "metadata" : { "message" : values, "code" : 400 }
  }

  res.status(400).json(data);
  res.end();
}

exports.badSql = function(values, res) {
  var data = {
      "response" : { "data" : null },
      "metadata" : { "code" : values.errno, "sqlState": values.sqlState, "message" : ( values.sqlMessage ).split(';')[0] }
  }

  res.status(400).json(data);
  res.end();
}

exports.paramSalah = function(values, res) {
  var data = {
      "response" : { "data" : null },
      "metadata" : { "message" : values, "code" : 422 }
  }

  res.status(422).json(data);
  res.end();
}


