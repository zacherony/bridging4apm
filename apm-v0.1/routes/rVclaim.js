const { 
    createSEPv2, 
    getPesertaByNoka, 
    getPesertaByNik,
    getRujukanByNokaSingle,
    getRujukanByNoruj, 
    getRefPoli,
    getRefdx,
    getRefdpjp, 
    getJadwalDokter 
} = require('../controllers/cSep')

module.exports = function(app) {
    
    app.get("/api/vclaim/peserta/noka/:noka/tgl/:tgl", getPesertaByNoka)
    app.get("/api/vclaim/peserta/nik/:nik/tgl/:tgl", getPesertaByNik)

    app.get("/api/vclaim/rujukan/noka/:noka", getRujukanByNokaSingle)
    app.get("/api/vclaim/rujukan/norujukan/:noruj", getRujukanByNoruj)

    app.get("/api/vclaim/ref/poli/:poli", getRefPoli)
    app.get("/api/vclaim/ref/dpjp/:kddpjp/jenpel/:jenpel/tgl/:tgl", getRefdpjp)
    app.get("/api/vclaim/ref/diagnosa/:dx", getRefdx)

    app.get("/api/antrol/jadwal/:kdpoli/tgl/:tgl", getJadwalDokter)

    app.post("/api/vclaim/SEPv2/insert", createSEPv2)

}

