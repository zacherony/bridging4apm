const { 
    createSEPv2,
    getPesertaByNoka, 
    getPesertaByNik,
    getRujukanByNokaSingle,
    getRujukanByNoruj, 
    getRefPoli,
    getRefdx,
    getRefdpjp, 
    getJadwalDokter 
} = require('../controllers/cSep')

module.exports = function(app) {
    
    app.get("/api/vclaim/peserta/noka", getPesertaByNoka)
    app.get("/api/vclaim/peserta/nik", getPesertaByNik)

    app.get("/api/vclaim/rujukan/noka", getRujukanByNokaSingle)
    app.get("/api/vclaim/rujukan/norujukan", getRujukanByNoruj)

    app.get("/api/vclaim/ref/poli", getRefPoli)
    app.get("/api/vclaim/ref/dpjp", getRefdpjp)
    app.get("/api/vclaim/ref/diagnosa", getRefdx)

    app.get("/api/antrol/jadwal", getJadwalDokter)

    app.post("/api/vclaim/SEPv2/insert", createSEPv2)

}

